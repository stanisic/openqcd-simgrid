#+TITLE: SimGrid execution of OpenQCD code on hpls machine
#+DATE: mercredi 11 mai 2016, 19:52:36 (UTC+0200)
#+AUTHOR: stanisic
#+MACHINE: hpls
#+FILE: SimOpenQCDData0.org
 
* MACHINE INFO:
** PEOPLE LOGGED WHEN EXPERIMENT STARTED:
stanisic :0           2016-05-11 11:23 (:0)
stanisic pts/17       2016-05-11 11:24 (:0)
stanisic pts/18       2016-05-11 17:10 (:0)
stanisic pts/2        2016-05-11 16:58 (:0)
stanisic pts/19       2016-05-11 17:38 (:0)
############################################
** ENVIRONMENT VARIABLES:
XDG_VTNR=7
LC_PAPER=fr_FR.UTF-8
MPI_INCLUDE=/usr/include/mpi
LC_ADDRESS=fr_FR.UTF-8
XDG_SESSION_ID=c2
XDG_GREETER_DATA_DIR=/var/lib/lightdm-data/stanisic
LC_MONETARY=fr_FR.UTF-8
CLUTTER_IM_MODULE=xim
GPG_AGENT_INFO=/tmp/gpg-NsxhAs/S.gpg-agent:2285:1
TERM=xterm-256color
VTE_VERSION=4002
SHELL=/bin/bash
WINDOWID=60822610
LC_NUMERIC=fr_FR.UTF-8
UPSTART_SESSION=unix:abstract=/com/ubuntu/upstart-session/1000/2163
GNOME_KEYRING_CONTROL=
GTK_MODULES=unity-gtk-module
USER=stanisic
LS_COLORS=rs=0:di=01;34:ln=01;36:mh=00:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:su=37;41:sg=30;43:ca=30;41:tw=30;42:ow=34;42:st=37;44:ex=01;32:*.tar=01;31:*.tgz=01;31:*.arc=01;31:*.arj=01;31:*.taz=01;31:*.lha=01;31:*.lz4=01;31:*.lzh=01;31:*.lzma=01;31:*.tlz=01;31:*.txz=01;31:*.tzo=01;31:*.t7z=01;31:*.zip=01;31:*.z=01;31:*.Z=01;31:*.dz=01;31:*.gz=01;31:*.lrz=01;31:*.lz=01;31:*.lzo=01;31:*.xz=01;31:*.bz2=01;31:*.bz=01;31:*.tbz=01;31:*.tbz2=01;31:*.tz=01;31:*.deb=01;31:*.rpm=01;31:*.jar=01;31:*.war=01;31:*.ear=01;31:*.sar=01;31:*.rar=01;31:*.alz=01;31:*.ace=01;31:*.zoo=01;31:*.cpio=01;31:*.7z=01;31:*.rz=01;31:*.cab=01;31:*.jpg=01;35:*.jpeg=01;35:*.gif=01;35:*.bmp=01;35:*.pbm=01;35:*.pgm=01;35:*.ppm=01;35:*.tga=01;35:*.xbm=01;35:*.xpm=01;35:*.tif=01;35:*.tiff=01;35:*.png=01;35:*.svg=01;35:*.svgz=01;35:*.mng=01;35:*.pcx=01;35:*.mov=01;35:*.mpg=01;35:*.mpeg=01;35:*.m2v=01;35:*.mkv=01;35:*.webm=01;35:*.ogm=01;35:*.mp4=01;35:*.m4v=01;35:*.mp4v=01;35:*.vob=01;35:*.qt=01;35:*.nuv=01;35:*.wmv=01;35:*.asf=01;35:*.rm=01;35:*.rmvb=01;35:*.flc=01;35:*.avi=01;35:*.fli=01;35:*.flv=01;35:*.gl=01;35:*.dl=01;35:*.xcf=01;35:*.xwd=01;35:*.yuv=01;35:*.cgm=01;35:*.emf=01;35:*.axv=01;35:*.anx=01;35:*.ogv=01;35:*.ogx=01;35:*.aac=00;36:*.au=00;36:*.flac=00;36:*.m4a=00;36:*.mid=00;36:*.midi=00;36:*.mka=00;36:*.mp3=00;36:*.mpc=00;36:*.ogg=00;36:*.ra=00;36:*.wav=00;36:*.axa=00;36:*.oga=00;36:*.spx=00;36:*.xspf=00;36:
LC_TELEPHONE=fr_FR.UTF-8
XDG_SESSION_PATH=/org/freedesktop/DisplayManager/Session0
XDG_SEAT_PATH=/org/freedesktop/DisplayManager/Seat0
SSH_AUTH_SOCK=/run/user/1000/keyring/ssh
DEFAULTS_PATH=/usr/share/gconf/ubuntu.default.path
XDG_CONFIG_DIRS=/etc/xdg/xdg-ubuntu:/usr/share/upstart/xdg:/etc/xdg
DESKTOP_SESSION=ubuntu
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games
QT_IM_MODULE=ibus
QT_QPA_PLATFORMTHEME=appmenu-qt5
LC_IDENTIFICATION=fr_FR.UTF-8
XDG_SESSION_TYPE=x11
PWD=/home/stanisic/Work/openqcd-study
JOB=gnome-session
XMODIFIERS=@im=ibus
EDITOR=/usr/bin/emacs
GNOME_KEYRING_PID=
LANG=en_US.UTF-8
GDM_LANG=en_US
MANDATORY_PATH=/usr/share/gconf/ubuntu.mandatory.path
LC_MEASUREMENT=fr_FR.UTF-8
COMPIZ_CONFIG_PROFILE=ubuntu
IM_CONFIG_PHASE=1
GDMSESSION=ubuntu
SESSIONTYPE=gnome-session
GTK2_MODULES=overlay-scrollbar
SHLVL=3
HOME=/home/stanisic
XDG_SEAT=seat0
LANGUAGE=en_US
GNOME_DESKTOP_SESSION_ID=this-is-deprecated
CFLAGS= -std=c99 -D_XOPEN_SOURCE=500 -g
UPSTART_INSTANCE=
GCC=/usr/bin/gcc
UPSTART_EVENTS=started starting
XDG_SESSION_DESKTOP=ubuntu
LOGNAME=stanisic
DBUS_SESSION_BUS_ADDRESS=unix:abstract=/tmp/dbus-9iP4b8gdwy
XDG_DATA_DIRS=/usr/share/ubuntu:/usr/share/gnome:/usr/local/share/:/usr/share/
QT4_IM_MODULE=xim
MPI_HOME=/usr
LESSOPEN=| /usr/bin/lesspipe %s
INSTANCE=Unity
UPSTART_JOB=unity-settings-daemon
XDG_RUNTIME_DIR=/run/user/1000
DISPLAY=:0
CC=/home/stanisic/Work/simgrid/build/bin/smpicc
XDG_CURRENT_DESKTOP=Unity
GTK_IM_MODULE=ibus
LESSCLOSE=/usr/bin/lesspipe %s %s
LC_TIME=fr_FR.UTF-8
LC_NAME=fr_FR.UTF-8
XAUTHORITY=/home/stanisic/.Xauthority
_=/usr/bin/env
############################################
** HOSTNAME:
hpls
############################################
** MEMORY HIERARCHY:
Machine (7859MB)
  Package L#0 + L3 L#0 (4096KB)
    L2 L#0 (256KB) + L1d L#0 (32KB) + L1i L#0 (32KB) + Core L#0
      PU L#0 (P#0)
      PU L#1 (P#1)
    L2 L#1 (256KB) + L1d L#1 (32KB) + L1i L#1 (32KB) + Core L#1
      PU L#2 (P#2)
      PU L#3 (P#3)
  HostBridge L#0
    PCI 8086:1616
      GPU L#0 "card0"
      GPU L#1 "renderD128"
      GPU L#2 "controlD64"
    PCI 8086:15a2
      Net L#3 "enp0s25"
    PCIBridge
      PCI 8086:095a
        Net L#4 "wlo1"
    PCI 8086:9c83
      Block(Disk) L#5 "sda"
############################################
** CPU INFO:
processor	: 0
vendor_id	: GenuineIntel
cpu family	: 6
model		: 61
model name	: Intel(R) Core(TM) i7-5600U CPU @ 2.60GHz
stepping	: 4
microcode	: 0x1f
cpu MHz		: 2299.984
cache size	: 4096 KB
physical id	: 0
siblings	: 4
core id		: 0
cpu cores	: 2
apicid		: 0
initial apicid	: 0
fpu		: yes
fpu_exception	: yes
cpuid level	: 20
wp		: yes
flags		: fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush dts acpi mmx fxsr sse sse2 ss ht tm pbe syscall nx pdpe1gb rdtscp lm constant_tsc arch_perfmon pebs bts rep_good nopl xtopology nonstop_tsc aperfmperf eagerfpu pni pclmulqdq dtes64 monitor ds_cpl vmx smx est tm2 ssse3 fma cx16 xtpr pdcm pcid sse4_1 sse4_2 x2apic movbe popcnt tsc_deadline_timer aes xsave avx f16c rdrand lahf_lm abm 3dnowprefetch ida arat epb pln pts dtherm intel_pt tpr_shadow vnmi flexpriority ept vpid fsgsbase tsc_adjust bmi1 hle avx2 smep bmi2 erms invpcid rtm rdseed adx smap xsaveopt
bugs		:
bogomips	: 5187.75
clflush size	: 64
cache_alignment	: 64
address sizes	: 39 bits physical, 48 bits virtual
power management:

processor	: 1
vendor_id	: GenuineIntel
cpu family	: 6
model		: 61
model name	: Intel(R) Core(TM) i7-5600U CPU @ 2.60GHz
stepping	: 4
microcode	: 0x1f
cpu MHz		: 2726.648
cache size	: 4096 KB
physical id	: 0
siblings	: 4
core id		: 0
cpu cores	: 2
apicid		: 1
initial apicid	: 1
fpu		: yes
fpu_exception	: yes
cpuid level	: 20
wp		: yes
flags		: fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush dts acpi mmx fxsr sse sse2 ss ht tm pbe syscall nx pdpe1gb rdtscp lm constant_tsc arch_perfmon pebs bts rep_good nopl xtopology nonstop_tsc aperfmperf eagerfpu pni pclmulqdq dtes64 monitor ds_cpl vmx smx est tm2 ssse3 fma cx16 xtpr pdcm pcid sse4_1 sse4_2 x2apic movbe popcnt tsc_deadline_timer aes xsave avx f16c rdrand lahf_lm abm 3dnowprefetch ida arat epb pln pts dtherm intel_pt tpr_shadow vnmi flexpriority ept vpid fsgsbase tsc_adjust bmi1 hle avx2 smep bmi2 erms invpcid rtm rdseed adx smap xsaveopt
bugs		:
bogomips	: 5187.75
clflush size	: 64
cache_alignment	: 64
address sizes	: 39 bits physical, 48 bits virtual
power management:

processor	: 2
vendor_id	: GenuineIntel
cpu family	: 6
model		: 61
model name	: Intel(R) Core(TM) i7-5600U CPU @ 2.60GHz
stepping	: 4
microcode	: 0x1f
cpu MHz		: 3056.929
cache size	: 4096 KB
physical id	: 0
siblings	: 4
core id		: 1
cpu cores	: 2
apicid		: 2
initial apicid	: 2
fpu		: yes
fpu_exception	: yes
cpuid level	: 20
wp		: yes
flags		: fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush dts acpi mmx fxsr sse sse2 ss ht tm pbe syscall nx pdpe1gb rdtscp lm constant_tsc arch_perfmon pebs bts rep_good nopl xtopology nonstop_tsc aperfmperf eagerfpu pni pclmulqdq dtes64 monitor ds_cpl vmx smx est tm2 ssse3 fma cx16 xtpr pdcm pcid sse4_1 sse4_2 x2apic movbe popcnt tsc_deadline_timer aes xsave avx f16c rdrand lahf_lm abm 3dnowprefetch ida arat epb pln pts dtherm intel_pt tpr_shadow vnmi flexpriority ept vpid fsgsbase tsc_adjust bmi1 hle avx2 smep bmi2 erms invpcid rtm rdseed adx smap xsaveopt
bugs		:
bogomips	: 5187.75
clflush size	: 64
cache_alignment	: 64
address sizes	: 39 bits physical, 48 bits virtual
power management:

processor	: 3
vendor_id	: GenuineIntel
cpu family	: 6
model		: 61
model name	: Intel(R) Core(TM) i7-5600U CPU @ 2.60GHz
stepping	: 4
microcode	: 0x1f
cpu MHz		: 3086.585
cache size	: 4096 KB
physical id	: 0
siblings	: 4
core id		: 1
cpu cores	: 2
apicid		: 3
initial apicid	: 3
fpu		: yes
fpu_exception	: yes
cpuid level	: 20
wp		: yes
flags		: fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush dts acpi mmx fxsr sse sse2 ss ht tm pbe syscall nx pdpe1gb rdtscp lm constant_tsc arch_perfmon pebs bts rep_good nopl xtopology nonstop_tsc aperfmperf eagerfpu pni pclmulqdq dtes64 monitor ds_cpl vmx smx est tm2 ssse3 fma cx16 xtpr pdcm pcid sse4_1 sse4_2 x2apic movbe popcnt tsc_deadline_timer aes xsave avx f16c rdrand lahf_lm abm 3dnowprefetch ida arat epb pln pts dtherm intel_pt tpr_shadow vnmi flexpriority ept vpid fsgsbase tsc_adjust bmi1 hle avx2 smep bmi2 erms invpcid rtm rdseed adx smap xsaveopt
bugs		:
bogomips	: 5187.75
clflush size	: 64
cache_alignment	: 64
address sizes	: 39 bits physical, 48 bits virtual
power management:

############################################
** CPU GOVERNOR:
powersave
############################################
** CPU FREQUENCY:
2299984
############################################
** LINUX AND GCC VERSIONS:
Linux version 4.2.0-36-generic (buildd@lgw01-25) (gcc version 5.2.1 20151010 (Ubuntu 5.2.1-22ubuntu2) ) #41-Ubuntu SMP Mon Apr 18 15:49:10 UTC 2016
############################################
* CODE REVISIONS:
** GIT REVISION OF REPOSITORY:
commit f9eb795cef1bc596ca963a0f9f62bde9faea9180
Author: Luka Stanisic <luka.stanisic@inria.fr>
Date:   Wed May 11 19:51:37 2016 +0200

    additional info about input parameters
############################################
* INPUT PARAMETERS:
** /home/stanisic/Work/openqcd-study/openqcd_code/main/examples/ym1/ym1.in:
nstep        16

[MD trajectories]
nth          0
ntr          4
dtr_log      1
dtr_ms       1
dtr_cnfg     4
** /home/stanisic/Work/openqcd-study/openqcd_code/include/global.h:
#define NPROC0 2
#define NPROC1 1
#define NPROC2 1
#define NPROC3 1

#define L0 8
#define L1 8
#define L2 8
#define L3 8

#define NPROC0_BLK 1
#define NPROC1_BLK 1
#define NPROC2_BLK 1
#define NPROC3_BLK 1

#define NAME_SIZE 128
* COMPILATION:
/home/stanisic/Work/simgrid/build/bin/smpicc ym1.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/archive/archive.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/archive/sarchive.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/block/block.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/block/blk_grid.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/block/map_u2blk.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/block/map_sw2blk.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/block/map_s2blk.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/dfl/dfl_geometry.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/dfl/dfl_subspace.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/dfl/ltl_gcr.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/dfl/dfl_sap_gcr.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/dfl/dfl_modes.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/dirac/Dw_dble.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/dirac/Dw.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/dirac/Dw_bnd.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/flags/flags.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/flags/action_parms.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/flags/dfl_parms.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/flags/force_parms.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/flags/hmc_parms.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/flags/lat_parms.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/flags/mdint_parms.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/flags/rat_parms.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/flags/rw_parms.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/flags/sap_parms.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/flags/solver_parms.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/forces/force0.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/forces/force1.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/forces/force2.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/forces/force3.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/forces/force4.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/forces/force5.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/forces/frcfcts.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/forces/genfrc.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/forces/tmcg.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/forces/tmcgm.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/forces/xtensor.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/lattice/bcnds.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/lattice/uidx.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/lattice/ftidx.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/lattice/geometry.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/linalg/salg.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/linalg/salg_dble.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/linalg/valg.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/linalg/valg_dble.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/linalg/liealg.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/linalg/cmatrix_dble.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/linalg/cmatrix.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/linsolv/cgne.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/linsolv/mscg.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/linsolv/fgcr.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/linsolv/fgcr4vd.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/little/Aw_gen.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/little/Aw_com.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/little/Aw_ops.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/little/Aw_dble.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/little/Aw.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/little/ltl_modes.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/mdflds/mdflds.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/mdflds/fcom.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/random/ranlux.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/random/ranlxs.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/random/ranlxd.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/random/gauss.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/ratfcts/elliptic.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/ratfcts/zolotarev.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/ratfcts/ratfcts.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/sap/sap_com.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/sap/sap_gcr.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/sap/sap.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/sap/blk_solv.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/sflds/sflds.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/sflds/scom.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/sflds/sdcom.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/sflds/Pbnd.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/sflds/Pbnd_dble.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/su3fcts/chexp.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/su3fcts/su3prod.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/su3fcts/su3ren.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/su3fcts/cm3x3.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/su3fcts/random_su3.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/sw_term/pauli.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/sw_term/pauli_dble.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/sw_term/swflds.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/sw_term/sw_term.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/tcharge/ftcom.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/tcharge/ftensor.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/tcharge/tcharge.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/tcharge/ym_action.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/uflds/plaq_sum.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/uflds/uflds.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/uflds/udcom.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/uflds/bstap.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/update/chrono.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/update/mdsteps.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/update/counters.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/update/mdint.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/update/hmc.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/update/rwtm.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/update/rwtmeo.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/openqcd-study/openqcd_code/modules/update/hmc.c:85:0: warning: "MAX" redefined
 #define MAX(n,m) \
 ^
In file included from /home/stanisic/Work/simgrid/build/include/xbt/misc.h:13:0,
                 from /home/stanisic/Work/simgrid/build/include/smpi/smpi.h:19,
                 from /home/stanisic/Work/simgrid/build/include/smpi/mpi.h:14,
                 from /home/stanisic/Work/openqcd-study/openqcd_code/modules/update/hmc.c:70:
/home/stanisic/Work/simgrid/build/include/xbt/base.h:131:0: note: this is the location of the previous definition
 #define MAX(a,b) ((a)>(b)?(a):(b))
 ^
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/update/rwrat.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/utils/endian.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/utils/mutils.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/utils/utils.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/utils/wspace.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/vflds/vflds.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/vflds/vinit.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/vflds/vcom.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/vflds/vdcom.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ../modules/wflow/wflow.c -c  -std=c99 -D_XOPEN_SOURCE=500 -g  -I/usr/include/mpi -I../include
/home/stanisic/Work/simgrid/build/bin/smpicc ym1.o archive.o sarchive.o block.o blk_grid.o map_u2blk.o map_sw2blk.o map_s2blk.o dfl_geometry.o dfl_subspace.o ltl_gcr.o dfl_sap_gcr.o dfl_modes.o Dw_dble.o Dw.o Dw_bnd.o flags.o action_parms.o dfl_parms.o force_parms.o hmc_parms.o lat_parms.o mdint_parms.o rat_parms.o rw_parms.o sap_parms.o solver_parms.o force0.o force1.o force2.o force3.o force4.o force5.o frcfcts.o genfrc.o tmcg.o tmcgm.o xtensor.o bcnds.o uidx.o ftidx.o geometry.o salg.o salg_dble.o valg.o valg_dble.o liealg.o cmatrix_dble.o cmatrix.o cgne.o mscg.o fgcr.o fgcr4vd.o Aw_gen.o Aw_com.o Aw_ops.o Aw_dble.o Aw.o ltl_modes.o mdflds.o fcom.o ranlux.o ranlxs.o ranlxd.o gauss.o elliptic.o zolotarev.o ratfcts.o sap_com.o sap_gcr.o sap.o blk_solv.o sflds.o scom.o sdcom.o Pbnd.o Pbnd_dble.o chexp.o su3prod.o su3ren.o cm3x3.o random_su3.o pauli.o pauli_dble.o swflds.o sw_term.o ftcom.o ftensor.o tcharge.o ym_action.o plaq_sum.o uflds.o udcom.o bstap.o chrono.o mdsteps.o counters.o mdint.o hmc.o rwtm.o rwtmeo.o rwrat.o endian.o mutils.o utils.o wspace.o vflds.o vinit.o vcom.o vdcom.o wflow.o  -std=c99 -D_XOPEN_SOURCE=500 -g  \
        -L/usr/lib -lm -o ym1
############################################
* RUNNING OPTIONS:
/home/stanisic/Work/simgrid/build/bin/smpirun -platform /home/stanisic/Work/git_gforge/starpu-simgrid/src/starpu-mpi/cluster.xml -hostfile /home/stanisic/Work/git_gforge/starpu-simgrid/src/starpu-mpi/hostfile --cfg=smpi/privatize_global_variables:yes -np 2 /home/stanisic/Work/openqcd-study/openqcd_code/main/ym1 -i /home/stanisic/Work/openqcd-study/openqcd_code/main/examples/ym1/ym1.in -noloc -noms
############################################
* ELAPSED TIME:
Elapsed:    54.065750992 seconds
* STDERR OUTPUT:
[0.000000] [xbt_cfg/INFO] Configuration change: Set 'surf/precision' to '1e-9'
[0.000000] [xbt_cfg/INFO] Configuration change: Set 'network/model' to 'SMPI'
[0.000000] [xbt_cfg/INFO] Configuration change: Set 'network/TCP_gamma' to '4194304'
[0.000000] [xbt_cfg/INFO] Configuration change: Set 'smpi/privatize_global_variables' to 'yes'
[0.000000] /home/stanisic/Work/simgrid/src/xbt/config.c:240: [xbt_cfg/WARNING] Config elem tracing/filename registered twice.
[0.000000] /home/stanisic/Work/simgrid/src/xbt/config.c:240: [xbt_cfg/WARNING] Config elem tracing registered twice.
[0.000000] /home/stanisic/Work/simgrid/src/xbt/config.c:240: [xbt_cfg/WARNING] Config elem tracing/platform registered twice.
[0.000000] /home/stanisic/Work/simgrid/src/xbt/config.c:240: [xbt_cfg/WARNING] Config elem tracing/platform/topology registered twice.
[0.000000] /home/stanisic/Work/simgrid/src/xbt/config.c:240: [xbt_cfg/WARNING] Config elem tracing/smpi registered twice.
[0.000000] /home/stanisic/Work/simgrid/src/xbt/config.c:240: [xbt_cfg/WARNING] Config elem tracing/smpi/group registered twice.
[0.000000] /home/stanisic/Work/simgrid/src/xbt/config.c:240: [xbt_cfg/WARNING] Config elem tracing/smpi/computing registered twice.
[0.000000] /home/stanisic/Work/simgrid/src/xbt/config.c:240: [xbt_cfg/WARNING] Config elem tracing/smpi/sleeping registered twice.
[0.000000] /home/stanisic/Work/simgrid/src/xbt/config.c:240: [xbt_cfg/WARNING] Config elem tracing/smpi/internals registered twice.
[0.000000] /home/stanisic/Work/simgrid/src/xbt/config.c:240: [xbt_cfg/WARNING] Config elem tracing/categorized registered twice.
[0.000000] /home/stanisic/Work/simgrid/src/xbt/config.c:240: [xbt_cfg/WARNING] Config elem tracing/uncategorized registered twice.
[0.000000] /home/stanisic/Work/simgrid/src/xbt/config.c:240: [xbt_cfg/WARNING] Config elem tracing/msg/process registered twice.
[0.000000] /home/stanisic/Work/simgrid/src/xbt/config.c:240: [xbt_cfg/WARNING] Config elem tracing/msg/vm registered twice.
[0.000000] /home/stanisic/Work/simgrid/src/xbt/config.c:240: [xbt_cfg/WARNING] Config elem tracing/disable_link registered twice.
[0.000000] /home/stanisic/Work/simgrid/src/xbt/config.c:240: [xbt_cfg/WARNING] Config elem tracing/disable_power registered twice.
[0.000000] /home/stanisic/Work/simgrid/src/xbt/config.c:240: [xbt_cfg/WARNING] Config elem tracing/buffer registered twice.
[0.000000] /home/stanisic/Work/simgrid/src/xbt/config.c:240: [xbt_cfg/WARNING] Config elem tracing/onelink_only registered twice.
[0.000000] /home/stanisic/Work/simgrid/src/xbt/config.c:240: [xbt_cfg/WARNING] Config elem tracing/disable_destroy registered twice.
[0.000000] /home/stanisic/Work/simgrid/src/xbt/config.c:240: [xbt_cfg/WARNING] Config elem tracing/basic registered twice.
[0.000000] /home/stanisic/Work/simgrid/src/xbt/config.c:240: [xbt_cfg/WARNING] Config elem tracing/smpi/display_sizes registered twice.
[0.000000] /home/stanisic/Work/simgrid/src/xbt/config.c:240: [xbt_cfg/WARNING] Config elem tracing/smpi/format registered twice.
[0.000000] /home/stanisic/Work/simgrid/src/xbt/config.c:240: [xbt_cfg/WARNING] Config elem tracing/smpi/format/ti_one_file registered twice.
[0.000000] /home/stanisic/Work/simgrid/src/xbt/config.c:240: [xbt_cfg/WARNING] Config elem tracing/comment registered twice.
[0.000000] /home/stanisic/Work/simgrid/src/xbt/config.c:240: [xbt_cfg/WARNING] Config elem tracing/comment_file registered twice.
[0.000000] /home/stanisic/Work/simgrid/src/xbt/config.c:240: [xbt_cfg/WARNING] Config elem tracing/precision registered twice.
[0.000000] /home/stanisic/Work/simgrid/src/xbt/config.c:240: [xbt_cfg/WARNING] Config elem viva/uncategorized registered twice.
[0.000000] /home/stanisic/Work/simgrid/src/xbt/config.c:240: [xbt_cfg/WARNING] Config elem viva/categorized registered twice.
[0.000000] [smpi_kernel/INFO] You did not set the power of the host running the simulation.  The timings will certainly not be accurate.  Use the option "--cfg=smpi/running_power:<flops>" to set its value.Check http://simgrid.org/simgrid/latest/doc/options.html#options_smpi_bench for more information.
* STDOUT OUTPUT:
* LOG FILE:

Simulation of the SU(3) gauge theory
------------------------------------

New run, start from random configuration

Using the HMC algorithm
Program version openQCD-1.4
The machine is little endian
The local disks are not used

16x8x8x8 lattice, 8x8x8x8 local lattice
2x1x1x1 process grid, 1x1x1x1 process block size

beta = 6.0
c0 = 1.6667, c1 = -0.0833375

Open-SF boundary conditions
cG = 1.1
cG' = 1.05
phi' = 0.92,0.76,-1.68

Random number generator:
level = 0, seed = 73099

Trajectories:
tau = 3.0
4th order OMF integrator
Number of steps = 16

nth = 0, ntr = 4
dtr_log = 1, dtr_cnfg = 4

Wilson flow observables are not measured

Trajectory no 1
dH = -1.2e+00, iac = 1
Average plaquette = 0.894223
Acceptance rate = 1.00
Time per trajectory = 4.48e+00 sec (average = 4.48e+00 sec)

Trajectory no 2
dH = -6.4e-01, iac = 1
Average plaquette = 1.350844
Acceptance rate = 1.00
Time per trajectory = 4.44e+00 sec (average = 4.46e+00 sec)

Trajectory no 3
dH = -4.3e-01, iac = 1
Average plaquette = 1.675975
Acceptance rate = 1.00
Time per trajectory = 4.44e+00 sec (average = 4.45e+00 sec)

Trajectory no 4
dH = -2.4e-01, iac = 1
Average plaquette = 1.892936
Acceptance rate = 1.00
Time per trajectory = 4.44e+00 sec (average = 4.45e+00 sec)

Configuration no 1 exported

