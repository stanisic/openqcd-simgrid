#!/usr/bin/env bash
#Job name
#SBATCH -J mpi4
# Asking for N nodes
#SBATCH -N 4
# Output results message
#SBATCH -o %j.out
# Output error message
#SBATCH -e %j.err

# #SBATCH -p longq
module purge
module load slurm/14.03.0
module load compiler/gcc/5.3.0
module load mpich/ge/gcc/64/3.1.4

cd "/home-ext/stanisic/openqcd-study"

./run.sh -t -c -n 4 -v
