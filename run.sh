#!/bin/bash
# Script for running openQCD code in Native and SimGrid modes

set -e # fail fast

# Script parameters
basename="$PWD"
datafolder=""
dataname="OpenQCDData"
srcfolder="$basename/openqcd_code"

# Finding paths and flags
host="$(hostname | sed 's/[0-9]*//g' | cut -d'.' -f1)"
case ${host} in
    'hpls')
	export MPI_HOME=/usr
	export MPI_INCLUDE=/usr/include/mpi
	export GCC=/usr/bin/gcc
        export CC="$MPI_HOME/bin/mpicc"
	SIMGRID_PATH="/home/stanisic/Work/simgrid/build"
	;;
    'miriel')
	export MPI_INCLUDE=$MPI_HOME/include/
	export GCC=$(which gcc)
        export CC="$MPI_HOME/bin/mpicc -L/cm/shared/apps/slurm/14.03.0/lib64 -lpmi"
	SIMGRID_PATH="/home-ext/stanisic/scalfmm-study/simgrid/build"
	;;
    *)
        echo "New machine: be sure flags are correct!"
esac

# OpenQCD parameters
NP=2
NP0=2
NP1=1
NP2=1
NP3=1
GLOBALS="$srcfolder/include/global.h"
# Defining folders of the files
PROGRAM="$srcfolder/main"
INPUTFILE="$srcfolder/main/examples"
OUTPUTLOG="$srcfolder/data"

# SimGrid parameters
SMPIRUN="$SIMGRID_PATH/bin/smpirun"
PLATFORM="$basename/platforms/miriel_platform.xml"
HOSTFILE="$basename/platforms/miriel_hostfile16"

testing=0
simgrid_mode=0
recompile=0
program="ym1"
verbose=0
help_script()
{
    cat << EOF
Usage: $0 options

Script for running OpenQCD in Native and SimGrid modes

OPTIONS:
   -h      Show this message
   -t      Enable testing
   -s      Run SimGrid simulation
   -c      Recompile source code
   -d      Specify output data folder
   -n      Specify number of MPI nodes
   -p      Main program name (ym1 by default)
   -v      Verbose output
EOF
}
# Parsing options
while getopts "tscd:n:p:vh" opt; do
    case $opt in
	t)
	    testing=1
	    ;;
	s)
	    simgrid_mode=1
	    ;;
	c)
	    recompile=1
	    ;;
       	d)
	    datafolder="$basename/$OPTARG"
	    ;;
      	n)
	    NP="$OPTARG"
	    ;;
	p)
	    program="$OPTARG"
	    ;;
	v)
	    verbose=1
	    ;;
	h)
	    help_script
	    exit 4
	    ;;
	\?)
	    echo "Invalid option: -$OPTARG"
	    help_script
	    exit 3
	    ;;
    esac
done

#################################################################
# Doing "real" experiment, not testing
if [[ $testing == 0 ]]; then
# Checking if everything is commited
    if git diff-index --quiet HEAD --; then
	echo "Everything is commited"
    else
	echo "ATTENTION: Not everything is commited!"
    fi
fi

# Name of data folder where to store results if testing
if [[ $testing == 1 ]]; then
    datafolder="$basename/data/testing"
fi

# Producing the original name for output file
title="Native"
if [[ $simgrid_mode == 1 ]]; then
    dataname="Sim$dataname"
else
    dataname="Nat$dataname"
fi
bkup=0
while [ -e $datafolder/$dataname${bkup}.org ] || [ -e $datafolder/$dataname${bkup}.org~ ]; do 
    bkup=`expr $bkup + 1`
done
filedat="$datafolder/$dataname${bkup}.org"

# Getting main program name
export MAIN="$program"
PROGRAM="$PROGRAM/$program"
INPUTFILE="$INPUTFILE/$program/$program.in"
OUTPUTLOG="$OUTPUTLOG/$program/log/*.log"

##################################################
# Export flags
export CFLAGS=" -std=c99 -D_XOPEN_SOURCE=500 -g"
if [[ $simgrid_mode == 1 ]]; then
    export CC=$SIMGRID_PATH/bin/smpicc
fi

##################################################
# Starting to write output file and getting all metadata
title=""
if [[ $simgrid_mode == 1 ]]; then
    title="SimGrid"
else
    title="Native"    
fi
title="$title execution of OpenQCD code on $(eval hostname) machine"
./get_info.sh -t "$title" $filedat

##################################################
# Deleting previous execution
rm -rf $srcfolder/data
mkdir $srcfolder/data
mkdir $srcfolder/data/$program
mkdir $srcfolder/data/$program/log
mkdir $srcfolder/data/$program/dat
mkdir $srcfolder/data/$program/cnfg

#################################################################
# Computing number of processors per dimension (works until 16 MPI nodes)
if [[ $NP > 1 ]]; then
    NP0=2
fi
if [[ $NP > 2 ]]; then
    NP1=2
fi
if [[ $NP > 4 ]]; then
    NP2=2
fi
if [[ $NP > 8 ]]; then
    NP3=2
fi

##################################################
# Overwriting input parameters (problem size and number of lattices)
sed -i "/\#define NPROC0 /c\\#define NPROC0 $NP0" $GLOBALS
sed -i "/\#define NPROC1 /c\\#define NPROC1 $NP1" $GLOBALS
sed -i "/\#define NPROC2 /c\\#define NPROC2 $NP2" $GLOBALS
sed -i "/\#define NPROC3 /c\\#define NPROC3 $NP3" $GLOBALS

##################################################
# Reading all input parameters
echo "* INPUT PARAMETERS:" >> $filedat
echo "** $INPUTFILE:" >> $filedat
sed -n '32,39p' $INPUTFILE >> $filedat
echo "** $GLOBALS:" >> $filedat
sed -n '18,33p' $GLOBALS >> $filedat

##################################################
# Compilation
echo "* COMPILATION:" >> $filedat
if [[ $recompile == 1 ]]; then
    echo "Compiling..."
    cd $srcfolder/main
    make clean
    make -j5 $program >> $filedat 2>&1
else
    echo "used previous compilation" >> $filedat
fi
cd $srcfolder


##################################################
# Prepare running options
if [[ $simgrid_mode == 1 ]]; then
	running="$SMPIRUN -platform $PLATFORM -hostfile $HOSTFILE -np $NP --cfg=smpi/privatize_global_variables:yes --cfg=network/model:IB"
else
	running="mpirun"
fi
running="$running -np $NP $PROGRAM -i $INPUTFILE -noloc -noms"


#################################
# Run application
echo "############################################" >> $filedat
echo "* RUNNING OPTIONS:" >> $filedat
echo $running >> $filedat
echo "############################################" >> $filedat
echo "Executing..."
# Writing results
rm -f stdout.out
rm -f stderr.out
rm -f $srcfolder/stdout.out
rm -f $srcfolder/stderr.out
time1=$(date +%s.%N)
set +e # In order to detect and print execution errors
eval $running 1> stdout.out 2> stderr.out
set -e
time2=$(date +%s.%N)
echo "* ELAPSED TIME:" >> $filedat
echo "Elapsed:    $(echo "$time2 - $time1"|bc ) seconds" >> $filedat
echo "* STDERR OUTPUT:" >> $filedat
cat stderr.out >> $filedat
echo "* STDOUT OUTPUT:" >> $filedat
cat stdout.out >> $filedat
if [[ $verbose == 1 ]]; then
    cat stderr.out
    cat stdout.out
    cat $OUTPUTLOG
fi
rm -f stdout.out
rm -f stderr.out
rm -f $srcfolder/stdout.out
rm -f $srcfolder/stderr.out
rm -f $srcfolder/tmp*

cd $basename

##################################
# Here we ideally need to grep important values from the output file
# For now simply copy the log file
echo "* LOG FILE:" >> $filedat
cat $OUTPUTLOG >> $filedat
